﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
    public class Program
    {
        protected Program()
        {

        }

        static void Main(string[] args)
        {
            #region Task4 // Проверка первого задания модуля 3.2
            Task4 task4 = new Task4();
            Console.WriteLine("Введите натуральное число: ");
            int naturalNumber;
            task4.TryParseNaturalNumber(Console.ReadLine(), out naturalNumber);
            int[] array4 = task4.GetFibonacciSequence(naturalNumber);
            foreach (var item in array4)
            {
                Console.WriteLine(item);
            }
            #endregion

            #region Task5 // Проверка второго задания модуля 3.2
            Task5 task5 = new Task5();
            Console.WriteLine(task5.ReverseNumber(973206445));
            Console.WriteLine(task5.ReverseNumber(-973206445));

            #endregion

            #region Task6 // Проверка третьего задания модуля 3.2
            Task6 task6 = new Task6();
            int[] array6 = task6.GenerateArray(-2104017322);

            for (int i = 0; i < array6.Length; i++)
            {
                Console.Write("{0}\t", array6[i]);
            }

            task6.UpdateElementToOppositeSign(array6);

            Console.WriteLine();

            for (int i = 0; i < array6.Length; i++)
            {
                Console.Write("{0}\t", array6[i]);
            }
            #endregion

            #region Task 7 // Проверка четвертого задания модуля 3.2
            Task7 task7 = new Task7();
            int[] array7 = task7.GenerateArray(-2029589760);
            List<int> list = task7.FindElementGreaterThenPrevious(array7);

            for (int i = 0; i < array7.Length; i++)
            {
                Console.Write("{0}\t", array7[i]);
            }

            Console.WriteLine();

            foreach (var item in list)
            {
                Console.Write("{0}\t", item);
            }
            #endregion

            #region Task 8 // Проверка пятого задания модуля 3.2
            Task8 task8 = new Task8();

            int[,] array8 = task8.FillArrayInSpiral(0);

            for (int i = 0; i < array8.GetLength(0); i++)
            {
                for (int j = 0; j < array8.GetLength(0); j++)
                {
                    Console.Write("{0}\t", array8[i, j]);
                }
                Console.WriteLine();
            }
            #endregion
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if ((!int.TryParse(input, out result)) || (result < 0))
            {
                return false;
            }
            return true;
        }

        public int[] GetFibonacciSequence(int n)
        {
            if (n == 0)
                return new int[] { };
            if (n == 1)
                return new int[] { 0 };

            List<int> array = new List<int>() { 0, 1 };
            if (n == 2) return array.ToArray();

            for (int i = 2; i < n; i++)
            {
                array.Add(array[i - 1] + array[i - 2]);
            }
            return array.ToArray();
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            System.Text.StringBuilder reverseNumberString = new System.Text.StringBuilder();

            int endOfStringWithNumber = 0;
            if (sourceNumber <0)
            {
                endOfStringWithNumber = 1;
                reverseNumberString.Append("-");
            }

            System.Text.StringBuilder originNumberString = new System.Text.StringBuilder(sourceNumber.ToString());

            for (int i = originNumberString.Length - 1; i >= endOfStringWithNumber; i--)
            {
                reverseNumberString.Append(originNumberString[i]);
            }
            return Convert.ToInt32(reverseNumberString.ToString());
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 0)
                size = 0;
            Random random = new Random();
            int[] array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = random.Next(int.MinValue, int.MaxValue);
            }
            return array;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                source[i] = source[i] * -1;
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 0)
                size = 0;
            Random random = new Random();
            int[] array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = random.Next(int.MinValue, int.MaxValue);
            }
            return array;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> list = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                {
                    if (source[i] > source[i - 1])
                        list.Add(source[i]);
                }
            }

            return list;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] array = new int[size, size];
            int count = 1;
            int indent = 0;

            while (indent <= size / 2)
            {
                FillPerimeterSquare(ref array, indent, ref count);
                indent++;
            }

            if ((size % 2) > 0)
            {               
                array[size / 2, size /2] = count;
            }

            return array;
        }

        private static void FillPerimeterSquare(ref int[,] array, int indent, ref int count)
        {
            int size = array.GetLength(0) - 1;
            for (int i = indent; i < size - indent; i++)
            {
                array[indent, i] = count++;
            }
            for (int i = indent; i < size - indent; i++)
            {
                array[i, size - indent] = count++;
            }
            for (int i = size - indent; i > indent; i--)
            {
                array[size - indent, i] = count++;
            }
            for (int i = size - indent; i > indent; i--)
            {
                array[i, indent] = count++;
            }
        }

    }
}
